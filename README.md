Catalogus
===========

Dominican Catalogus for Drupal.

Instructions
------------

Unpack in the *modules* folder (currently in the root of your Drupal 8
installation) and enable in `/admin/modules`.

Then, after you have created your Provincial "community", visit `/admin/config/content/catalogus` 
and let the system know what ID it is.
