<?php

namespace Drupal\catalogus\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines CatalogusController class.
 */
class CatalogusController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function backOn($cid, $date) {
    return [
      '#type' => 'markup',
      '#markup' => t('Hello, World!') . "cid: $cid date: $date",
    ];
  }
}
