<?php

namespace Drupal\catalogus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CatalogusForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'catalogus_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('catalogus.settings');

    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('Special Communities'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['general']['province_community'] = [
      '#type' => 'number',
      '#title' => $this->t('Node ID number of the home province:'),
      '#default_value' => $config->get('catalogus.province_community'),
      '#description' => $this->t('Used for the Statistics page, and personal listings.'),
    ];
    $form['general']['hide_names'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node IDs of communities whose names should be ommitted in the print listings:'),
      '#default_value' => $config->get('catalogus.hide_names'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['general']['hide_communities'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node IDs of communities whose connections should be ommitted in the print listings:'),
      '#default_value' => $config->get('catalogus.hide_communities'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['general']['display_burial'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include "Buried in..." in the personal summary for people who have died'),
      '#default_value' => $config->get('catalogus.display_burial'),
    ];

    $form['personal_dates'] = array(
      '#type' => 'fieldset',
      '#title' => t('Personal Date Types'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['personal_dates']['birth_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Birth Date Type:'),
      '#default_value' => $config->get('catalogus.birth_tag'),
    ];
    $form['personal_dates']['begin_novitiate_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Begin Novitiate Date Type:'),
      '#default_value' => $config->get('catalogus.begin_novitiate_tag'),
    ];
    $form['personal_dates']['first_vows_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for First Vows Date Type:'),
      '#default_value' => $config->get('catalogus.first_vows_tag'),
    ];
    $form['personal_dates']['solemn_vows_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Solemn Vows Date Type:'),
      '#default_value' => $config->get('catalogus.solemn_vows_tag'),
    ];
    $form['personal_dates']['diaconal_ordination_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Diaconal Ordination Date Type:'),
      '#default_value' => $config->get('catalogus.diaconal_ordination_tag'),
    ];
    $form['personal_dates']['priesthood_ordination_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Priesthood Ordination Date Type:'),
      '#default_value' => $config->get('catalogus.priesthood_ordination_tag'),
    ];
    $form['personal_dates']['episcopal_ordination_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Episcopal Ordination Date Type:'),
      '#default_value' => $config->get('catalogus.episcopal_ordination_tag'),
    ];
    $form['personal_dates']['death_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Death Date Type:'),
      '#default_value' => $config->get('catalogus.death_tag'),
    ];

    $form['personal_names'] = array(
      '#type' => 'fieldset',
      '#title' => t('Personal Name Types'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['personal_names']['legal_name_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Legal Name Type:'),
      '#default_value' => $config->get('catalogus.legal_name_tag'),
    ];
    $form['personal_names']['religious_name_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Religious Name Type:'),
      '#default_value' => $config->get('catalogus.religious_name_tag'),
    ];
    $form['personal_names']['family_name_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Family Name Type:'),
      '#default_value' => $config->get('catalogus.family_name_tag'),
    ];

    $form['assignment_types'] = array(
      '#type' => 'fieldset',
      '#title' => t('Assignment Types'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['assignment_types']['member_of_province_assignment_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Member of Province Assignment Tag:'),
      '#default_value' => $config->get('catalogus.member_of_province_assignment_tag'),
    ];
    $form['assignment_types']['assigned_to_province_assignment_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID for Assigned to Province Assignment Tag:'),
      '#default_value' => $config->get('catalogus.assigned_to_province_assignment_tag'),
    ];
    $form['assignment_types']['simple_assignment_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Simply Assigned:'),
      '#default_value' => $config->get('catalogus.simple_assignment_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['ass_elsewhere_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Assigned but Living Elsewhere:'),
      '#default_value' => $config->get('catalogus.ass_elsewhere_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['in_residence_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for In Residence:'),
      '#default_value' => $config->get('catalogus.in_residence_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['ass_health_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Assigned but Living Elsewhere for Reasons of Health:'),
      '#default_value' => $config->get('catalogus.ass_health_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['ass_outside_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Assigned Outside of Home Province:'),
      '#default_value' => $config->get('catalogus.ass_outside_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['study_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Assigned for Reason of Study:'),
      '#default_value' => $config->get('catalogus.study_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['other_ass_tag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs for Other Assignment Types to List:'),
      '#default_value' => $config->get('catalogus.other_ass_tag'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['current_assignment_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs of Assignment Types considered as "primary":'),
      '#default_value' => $config->get('catalogus.current_assignment_types'),
      '#description' => $this->t('separate IDs with commas, order is important'),
    ];
    $form['assignment_types']['assignments_to_list_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs of Assignment Types to appear in personal summary listing:'),
      '#default_value' => $config->get('catalogus.assignments_to_list_types'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['house_assignment_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs of Priory Assignment Types to list separately:'),
      '#default_value' => $config->get('catalogus.house_assignment_types'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['exclaustrations'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs of Assignment Types that should be considered exclaustrated:'),
      '#default_value' => $config->get('catalogus.exclaustrations'),
      '#description' => $this->t('separate IDs with commas'),
    ];
    $form['assignment_types']['provincial'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID of Assignment Type for Provincial:'),
      '#default_value' => $config->get('catalogus.provincial'),
    ];

    $form['connections'] = array(
      '#type' => 'fieldset',
      '#title' => t('Special Connections'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['connections']['list_start_dates'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term IDs of Connections that you would like to list the start date:'),
      '#default_value' => $config->get('catalogus.list_start_dates'),
      '#description' => $this->t('separate IDs with commas'),
    ];

    $form['people_tags'] = array(
      '#type' => 'fieldset',
      '#title' => t('People Tags'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,  
    );
    $form['people_tags']['lay_brother_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID of the Lay Brother tag'),
      '#default_value' => $config->get('catalogus.lay_brother_tag'),
    ];
    $form['people_tags']['necrology_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID of the Necrology tag'),
      '#default_value' => $config->get('catalogus.necrology_tag'),
    ];
    $form['people_tags']['in_solemn_vows_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID of the In Solemn Vows tag'),
      '#default_value' => $config->get('catalogus.in_solemn_vows_tag'),
    ];
    $form['people_tags']['in_formation_tag'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID of the In Initial Formation tag'),
      '#default_value' => $config->get('catalogus.in_formation_tag'),
    ];

    return $form;
  }
  /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $config = $this->config('catalogus.settings');
      $config->set('catalogus.province_community', $form_state->getValue('province_community'));
      $config->set('catalogus.display_burial', $form_state->getValue('display_burial'));
      $config->set('catalogus.hide_names', str_replace(' ','',$form_state->getValue('hide_names')));
      $config->set('catalogus.hide_communities', str_replace(' ','',$form_state->getValue('hide_communities')));
      $config->set('catalogus.birth_tag', $form_state->getValue('birth_tag'));
      $config->set('catalogus.begin_novitiate_tag', $form_state->getValue('begin_novitiate_tag'));
      $config->set('catalogus.first_vows_tag', $form_state->getValue('first_vows_tag'));
      $config->set('catalogus.solemn_vows_tag', $form_state->getValue('solemn_vows_tag'));
      $config->set('catalogus.diaconal_ordination_tag', $form_state->getValue('diaconal_ordination_tag'));
      $config->set('catalogus.priesthood_ordination_tag', $form_state->getValue('priesthood_ordination_tag'));
      $config->set('catalogus.episcopal_ordination_tag', $form_state->getValue('episcopal_ordination_tag'));
      $config->set('catalogus.death_tag', $form_state->getValue('death_tag'));
      $config->set('catalogus.legal_name_tag', $form_state->getValue('legal_name_tag'));
      $config->set('catalogus.religious_name_tag', $form_state->getValue('religious_name_tag'));
      $config->set('catalogus.family_name_tag', $form_state->getValue('family_name_tag'));
      $config->set('catalogus.member_of_province_assignment_tag', str_replace(' ','',$form_state->getValue('member_of_province_assignment_tag')));
      $config->set('catalogus.assigned_to_province_assignment_tag', str_replace(' ','',$form_state->getValue('assigned_to_province_assignment_tag')));
      $config->set('catalogus.simple_assignment_tag', str_replace(' ','',$form_state->getValue('simple_assignment_tag')));
      $config->set('catalogus.ass_elsewhere_tag', str_replace(' ','',$form_state->getValue('ass_elsewhere_tag')));
      $config->set('catalogus.in_residence_tag', str_replace(' ','',$form_state->getValue('in_residence_tag')));
      $config->set('catalogus.ass_health_tag', str_replace(' ','',$form_state->getValue('ass_health_tag')));
      $config->set('catalogus.ass_outside_tag', str_replace(' ','',$form_state->getValue('ass_outside_tag')));
      $config->set('catalogus.other_ass_tag', str_replace(' ','',$form_state->getValue('other_ass_tag')));
      $config->set('catalogus.study_tag', str_replace(' ','',$form_state->getValue('study_tag')));
      $config->set('catalogus.current_assignment_types', str_replace(' ','',$form_state->getValue('current_assignment_types')));
      $config->set('catalogus.assignments_to_list_types', str_replace(' ','',$form_state->getValue('assignments_to_list_types')));
      $config->set('catalogus.house_assignment_types', str_replace(' ','',$form_state->getValue('house_assignment_types')));
      $config->set('catalogus.exclaustrations', str_replace(' ','',$form_state->getValue('exclaustrations')));
      $config->set('catalogus.provincial', $form_state->getValue('provincial'));
      $config->set('catalogus.list_start_dates', str_replace(' ','',$form_state->getValue('list_start_dates')));
      $config->set('catalogus.lay_brother_tag', $form_state->getValue('lay_brother_tag'));
      $config->set('catalogus.necrology_tag', $form_state->getValue('necrology_tag'));
      $config->set('catalogus.in_solemn_vows_tag', $form_state->getValue('in_solemn_vows_tag'));
      $config->set('catalogus.in_formation_tag', $form_state->getValue('in_formation_tag'));
      $config->save();
      return parent::submitForm($form, $form_state);
    }
    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
      return [
        'catalogus.settings',
      ];
    }

  }
